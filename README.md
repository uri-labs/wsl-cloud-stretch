# WSL Cloud / stretch

A [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/faq) distribution that includes:

* Debian 9.6 (stretch)
* Cloud Platform CLIs for Google Cloud Platform, AWS, and Azure
* Docker Client


## Installation


Download from https://bitbucket.com/uri-labs

1. Click 'wsl-cloud-stretch' 
1. Click 'Downloads'
1. Click 'Download repository'

1. Extract it to a directory on your system drive.

1. Double Click wslc-stretch.exe to launch the shell. 

Upon first launch, you'll need to wait approxiimately 5 minutes while the shell takes a few minutes to register and  extract the contents of the root file system. 

## Usage

**Set distribution as default**

>***Will allow VS Code and CMD to open WSL Cloud***


    wslconfig /s wslc-stretch



**List registered distributions**

    wslconfig /l

**Uninstall distribution**

>***This will completely remove the extracted files!***


    wslconfig /u  wslc-stretch






## WSL Prerequisites

* Windows 10

* WSL Enabled

**To enable WSL:**

Open PowerShell as Administrator and run:


    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux


Restart your computer when prompted.

## License

[MIT](https://bitbucket.org/uri-labs/wsl-cloud-stretch/raw/51ec2951782b4f40d1cdcc738374ae97ba28e282/LICENSE.md)

## Contributors 

Andrew Druffner, URI

## Dependencies / References

[WSLDL](https://github.com/yuk7/wsldl)
[WSL Distro Launcher Reference Implementation](https://github.com/Microsoft/WSL-DistroLauncher)